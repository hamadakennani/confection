# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError


class gakd_recharger_compte_client(models.TransientModel):
    _name = 'gakd.wizard.recharger.compte.client'
    _rec_name = 'id'
    _description = 'New Description'


    def _getClient_id(self):
        return self._context.get('active_ids',False)[0]

    @api.onchange('client_id')
    def _onchange_client(self):
	self.update({
		'solde': self.client_id.solde_compte_hide
	})


    client_id = fields.Many2one("res.partner",string="Client",default=_getClient_id)
    solde = fields.Float(string='Solde non affecté',readonly=True ,compute="_getSolde" ,store=True)
    montant = fields.Float(string='Montant à recharger')
    


    @api.multi
    def save_recharger(self):
	print "sssssssssssssssssss"
	print "xxxxxxxxxxxxxxxxxx"
	print "sssssssssssssssssss"
	#print self.

	if self.montant <= 0:
		raise ValidationError(_("Merci de renseigner un montant supérieur à zéro"))
	self.client_id.solde_compte_hide = self.client_id.solde_compte_hide + self.montant
        return {'type': 'ir.actions.act_window_close'}

# -------------------------------------------------


class gakd_recharger_carte(models.TransientModel):
    _name = 'gakd.wizard.recharger.carte'
    _rec_name = 'id'
    _description = 'New Description'


    def _getClient_id(self):
        return self._context.get('active_ids',False)[0]

    @api.onchange('client_id')
    def _onchange_client(self):
        self.update({
		'solde': self.client_id.solde_compte,
		'carte_ids': self.client_id.carte_ids
	})



    client_id = fields.Many2one("res.partner",string="Client",default=_getClient_id)
    solde = fields.Float(string='Solde non affecté',readonly=True)#,compute="_getSolde", store=True)
    carte_ids = fields.Many2many("gakd.carte", "wizard_recharger_carte", "recharge_id", "carte_id","Liste des Cartes")


    @api.multi
    def save_recharger(self):
        return {'type': 'ir.actions.act_window_close'}















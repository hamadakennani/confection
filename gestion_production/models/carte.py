# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from datetime import date, datetime, timedelta
import dateutil.relativedelta as relativedelta
from random import randint


class gakd_type_carte(models.Model):
    _name = 'gakd.type.carte'

    _rec_name = 'libelle'
    _description = ''

    #company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id )
    libelle = fields.Char(string="Libelle")
    front_carte = fields.Binary(string="Background d'avant")
    front_carte_name = fields.Char("Background d'avant")
    back_carte = fields.Binary(string="Background d'arrier")
    back_carte_name = fields.Char("Background d'arrier")
    state = fields.Selection([('activee','Actiée'),('suspendu','Suspendu'),('annule','Annulée')] , string="Statut" , default="activee")






class gakd_carte(models.Model):
    _name = 'gakd.carte'

    _rec_name = 'libelle'
    _description = ''


    @api.multi
    def setToBrouillon(self):
        self.ensure_one()
        self.state="brouillon"

    @api.multi
    def setToActivee(self):
        self.ensure_one()
        self.state="activee"
	self.date_activation=fields.datetime.now()

    @api.multi
    def setToGenere(self):
        self.ensure_one()
        self.state="generee"


    @api.multi
    def setToEnd(self):
        self.ensure_one()
        self.state="terminee"
	self.date_termine=fields.datetime.now()

    @api.multi
    def setToSuspendu(self):
        self.ensure_one()
        self.state="suspendu"
	self.date_suspension=fields.datetime.now()

    @api.multi
    def setToPerdu(self):
        self.ensure_one()
        self.state="perdu"
	self.date_perte=fields.datetime.now()

    @api.multi
    def setToExpire(self):
        self.ensure_one()
        self.state="expiree"
	self.date_expiration=fields.datetime.now()

    @api.multi
    def setToAnnule(self):
        self.ensure_one()
        self.state="annule"
	self.date_annulation=fields.datetime.now()



    def _default_serie(self):
	result = True
	serie_proposition = 0
        while result==True:
		serie_proposition = str(randint(111, 999)) + str(randint(111, 999)) + str(randint(111, 999))
		result = self.search([['num_serie', '=', int(serie_proposition)]]).id
        return serie_proposition

    def _getQrCode(self,string):
	import hashlib
	md5 = hashlib.md5()
	qrcode = ""
	result = True
	serie_proposition = 0
        while result==True:
		md5.update(string+str(serie_proposition))
		qrcode = md5.hexdigest()
		serie_proposition += 1 
		result = self.search([['qrcode', '=', qrcode ]]).id
	return qrcode




    #@api.depends('num_serie_hide')
    @api.onchange('libelle')
    def _send_serie(self):
	
	i = -1  # This could have been any integer, positive or negative
	today = fields.datetime.now().date()
#(self.create_date).strftime('%Y-%m-%d %H:%M:%S')


#date.strptime(self.create_date,'%Y-%m-%d %H:%M:%S') #date.today()

	nextyear = fields.datetime.now().replace(year=fields.datetime.now().year + 2)
	#nextyear = fields.date.now().replace(year=fields.date.now().year + 2)

	

    @api.constrains('solde')
    def _check_solde(self):
        if self.solde<0:
            raise ValidationError(_("Le solde de la carte devrait rester superieur ou égal à zéro"))
	#elif self.solde>self.owner_id.solde_compte:
        #    raise ValidationError(_("------------solde < solde owner ----------"))



    @api.multi
    def getCartByQrcode(self):
	print "======================"
	print "======================"
	print "======================"
	print "======================"
	print "======================"
	print "======================"
	print "======================"
	#carte = self.search([['qrcode', '=', qrcode]])
        
        return 88



    # ***************************************************************************************
    
    #company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id )
    owner_id = fields.Many2one("res.partner",string="Client", required=True)
    type_carte_id = fields.Many2one("gakd.type.carte",string="Type de carte", required=True)
    libelle = fields.Char(string="Libellé")
    num_serie = fields.Integer(string="Numéro de serie")
    code_pin = fields.Integer(string="code pin")
    qrcode = fields.Char(string="dddd")
    solde = fields.Float(string='Solde actuel')
    montant_affecter = fields.Float(string='Montant à affecter')
    date_activation = fields.Datetime("Date d'activation", readonly=True)
    date_perte = fields.Datetime("Date de perte", readonly=True)
    date_suspension = fields.Datetime("Date de suspension", readonly=True)
    date_expiration = fields.Date("Date d'expiration", readonly=True)
    date_termine = fields.Datetime("Date fin service", readonly=True)
    date_annulation = fields.Datetime("Date d'annulation", readonly=True)
    carte_consommation_ids = fields.One2many("gakd.carte.consommation","carte_id",string="Historiques", readonly=True)
    log_ids = fields.One2many("gakd.log","carte_id",string="Historiques", readonly=True)
    state = fields.Selection([('brouillon','Brouillon'),('generee','Générée'),('activee','Actiée'),('suspendu','Suspendu'),('perdu','Perdu'),('expiree','Expirée'),('terminee','Terminée'),('annule','Annulée')],string="Statut",default="brouillon")



    def SendMail(self,mailsto,Subject,body):
	import smtplib
	from email.MIMEMultipart import MIMEMultipart
	from email.MIMEText import MIMEText
	mail_server = self.env['ir.mail_server'].search([["name","=","localhost"]])

#self.pool.get('ir.mail_server').browse(cr,uid,self.pool.get('ir.mail_server').search(cr, uid, [('name','=','Contencia-SOFT')])[0])
	msg = MIMEMultipart()
	msg.set_charset("utf-8")
	msg['From']    = mail_server.smtp_user
	msg['To']      = mailsto
	msg['Subject'] = Subject
	body = body
	msg.attach(MIMEText(body, 'html'))
	server = smtplib.SMTP(mail_server.smtp_host, 587)
	server.ehlo()
	server.starttls()
	server.ehlo()
	server.login(mail_server.smtp_user,mail_server.smtp_pass)
	text = msg.as_string()
	server.sendmail(mail_server.smtp_host, mailsto.split(','), text)
	return True

    @api.model
    def create(self, vals):

	if vals.get("montant_affecter",False) and vals.get("montant_affecter",False)>0:
		vals['solde'] = vals.get("montant_affecter",False)
		vals['montant_affecter'] = 0
	#elif vals.get("montant_affecter",False)<0:
	#	raise ValidationError(_("------------solde affecter au carte > 0 ----------"))

	today = fields.datetime.now().date()
	vals['date_expiration'] = today.replace(year=today.year + 2)
        vals['num_serie'] = self._default_serie()
	codepin = randint(1111, 9999)
        vals['code_pin'] = codepin
	qrcode = self._getQrCode(str(vals.get("num_serie"))+'<:>AKAD<:>'+str(vals.get("date_expiration")))
	if qrcode=="":
		raise ValidationError(_("Merci de contacter le prestataire pour la génération du QRCODE"))
	vals['qrcode'] = qrcode
	carteResult = super(gakd_carte, self).create(vals)
	
	
	body = """Bonjour,
		Veuillez noter que le code PIN de votre carte """+vals.get('num_serie',False)+""" est : """+ str(codepin)

	Subject = "Carte JNP Pass"
	mailsto = self.env['res.partner'].search([["id","=",vals.get('owner_id',False)]]).email
	self.SendMail(mailsto,Subject,body)
	#raise ValidationError(_("Merci de contacter le prestataire pour la génération du QRCODE"))
        return carteResult





    @api.multi
    def write(self, vals):
	for carte in self:
		acteurName = self.env.user.name
		logs = []
		if vals.get("acteur_name",False):
			acteurName = vals.get("acteur_name",False)
		crt = self.search([['id', '=', carte.id]])
		if vals.get("libelle",False):
			logs.append(self.log(self.id,acteurName,self.libelle,vals.get("libelle",False),"Libellé"))
			
			
		if vals.get("montant_affecter",False):
			solde = crt.solde
			owner = crt.owner_id
			if vals.get("montant_affecter",False) > owner.solde_compte_hide:
				raise ValidationError(_("------------ Le solde affecté à la carte > au solde du compte client  ----------"))
			else:
				owner.solde_compte_hide = owner.solde_compte_hide - vals.get("montant_affecter",False)
			
			vals['solde'] = solde + vals.get("montant_affecter",False)
			vals['montant_affecter'] = 0
			logs.append(self.log(self.id,acteurName,str(self.solde),str(vals.get("solde",False)),"Solde"))
			
			if vals.get('solde',False)<0:
				raise ValidationError(_("Le solde de la carte devrait rester superieur ou égal à zéro"))
		if logs:
			vals['log_ids'] = logs
	
	return super(gakd_carte, self).write(vals)


    def log(self,ObjtId,acteurName,old_version,new_version,champ):
	return (0,0, {
				"carte_id" : ObjtId,
				"acteur_name" : acteurName,
				"old_version" : old_version,
				"new_version" : new_version,
				"champ" : champ,
			})



class gakd_carte_consommation(models.Model):
    _name = 'gakd.carte.consommation'

    _rec_name = 'carte_id'
    _description = ''

    #company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id )
    point_vente_id = fields.Many2one("gakd.point.vente",string="Point de vente", required=True)
    agent_id = fields.Many2one("gakd.agent",string="Agent")
    carte_id = fields.Many2one("gakd.carte",string="Carte", required=True)
    product_ids = fields.Many2one("product.product",string="Produit", required=True)
    quantite = fields.Integer(string='Quantité')
    montant = fields.Float(string='Montant')


class gakd_log(models.Model):
    _name = 'gakd.log'

    _rec_name = 'carte_id'
    _description = ''

    #company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id )

    acteur_name = fields.Char(string="Acteur")
    client_id = fields.Many2one("res.partner",string="Client")
    carte_id = fields.Many2one("gakd.carte",string="Carte")
    point_vente_id = fields.Many2one("gakd.point.vente",string="Point de vente")
    agent_id = fields.Many2one("gakd.agent",string="Agent")
    old_version = fields.Char(string="Ancien valeur")
    new_version = fields.Char(string="Nouvelle valeur")
    champ = fields.Char(string="Type de modification")


class gakd_point_vente(models.Model):
    _name = 'gakd.point.vente'

    _rec_name = 'libelle'
    _description = ''

    @api.multi
    def setToActive(self):
        self.ensure_one()
        self.state="activee"
	self.date_activation=fields.datetime.now()

    @api.multi
    def setToSuspendu(self):
        self.ensure_one()
        self.state="suspendu"
	self.date_suspension=fields.datetime.now()

    @api.multi
    def setToAnnule(self):
        self.ensure_one()
        self.state="annule"
	self.date_annulation=fields.datetime.now()

    #company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id )
    libelle = fields.Char(string="Libellé")
    adress = fields.Text(string="Adresse")
    historique_ids = fields.One2many("gakd.carte.consommation","carte_id",string="Historiques", readonly=True)
    agent_ids = fields.One2many("gakd.agent","point_vente_id",string="Agents")
    state = fields.Selection([('brouillon','Brouillon'),('activee','Actiée'),('suspendu','Suspendu'),('annule','Annulée')] , string="Statut" , default="activee")
    type = fields.Selection([('Boutique','Boutique'),('Station de service','Station de service') ] , string="Type" , default="Station de service")
    date_activation = fields.Datetime("Date d'activation", readonly=True)
    date_suspension = fields.Datetime("Date de suspension", readonly=True)
    date_annulation = fields.Datetime("Date d'annulation", readonly=True)
    log_ids = fields.One2many("gakd.log","point_vente_id",string="Historiques", readonly=True)
    transaction_ids = fields.One2many("gakd.carte.consommation","point_vente_id",string="Historiques", readonly=True)


    @api.multi
    def write(self, vals):
	for carte in self:
		logs = []
		acteurName = self.env.user.name
		if vals.get("acteur_name",False):
			acteurName = vals.get("acteur_name",False)
		
		if vals.get("libelle",False):
		   logs.append(self.log(self.id,acteurName,self.libelle,vals.get("libelle",False),"Libellé"))
		
		if vals.get("adress",False):
		   logs.append(self.log(self.id,acteurName,self.adress,vals.get("adress",False),"Adresse"))

		if vals.get("type",False):
		   logs.append(self.log(self.id,acteurName,self.type,vals.get("type",False),"Type"))


		if logs:
			vals['log_ids'] = logs
	
	return super(gakd_point_vente, self).write(vals)

    def log(self,ObjtId,acteurName,old_version,new_version,champ):
	return (0,0, {
				"point_vente_id" : ObjtId,
				"acteur_name" : acteurName,
				"old_version" : old_version,
				"new_version" : new_version,
				"champ" : champ,
			})


class gakd_agent(models.Model):
    _name = 'gakd.agent'

    _rec_name = 'name'
    _description = ''


    @api.multi
    def setToActive(self):
        self.ensure_one()
        self.state="activee"
	self.date_activation=fields.datetime.now()

    @api.multi
    def setToSuspendu(self):
        self.ensure_one()
        self.state="suspendu"
	self.date_suspension=fields.datetime.now()

    @api.multi
    def setToAnnule(self):
        self.ensure_one()
        self.state="annule"
	self.date_annulation=fields.datetime.now()

    #company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id )
    name = fields.Char(string="Nom")
    fonction = fields.Char(string="Fonction")
    matricule = fields.Char(string="Matricule")
    password = fields.Char(string="password")
    mail = fields.Char(string="Email")
    adress = fields.Text(string="Adresse")
    point_vente_id = fields.Many2one("gakd.point.vente",string="Point de vente")
    state = fields.Selection([('brouillon','Brouillon'),('activee','Actiée'),('suspendu','Suspendu'),('annule','Annulée') ] , string="Statut" , default="brouillon")


    date_activation = fields.Datetime("Date d'activation", readonly=True)
    date_suspension = fields.Datetime("Date de suspension", readonly=True)
    date_annulation = fields.Datetime("Date d'annulation", readonly=True)
    log_ids = fields.One2many("gakd.log","agent_id",string="Historiques", readonly=True)
    transaction_ids = fields.One2many("gakd.carte.consommation","agent_id",string="Historiques", readonly=True)



    @api.multi
    def write(self, vals):
	for carte in self:
		logs = []
		acteurName = self.env.user.name
		if vals.get("acteur_name",False):
			acteurName = vals.get("acteur_name",False)
		
		if vals.get("name",False):
		   logs.append(self.log(self.id,acteurName,self.name,vals.get("name",False),"Nom"))

		if vals.get("fonction",False):
		   logs.append(self.log(self.id,acteurName,self.fonction,vals.get("fonction",False),"Fonction"))

		if vals.get("matricule",False):
		   logs.append(self.log(self.id,acteurName,self.matricule,vals.get("matricule",False),"Matricule"))

		
		if vals.get("mail",False):
		   logs.append(self.log(self.id,acteurName,self.mail,vals.get("mail",False),"Email"))

		
		if vals.get("adress",False):
		   logs.append(self.log(self.id,acteurName,self.adress,vals.get("adress",False),"Adresse"))


		if logs:
			vals['log_ids'] = logs
	
	return super(gakd_agent, self).write(vals)

    def log(self,ObjtId,acteurName,old_version,new_version,champ):
	return (0,0, {
				"agent_id" : ObjtId,
				"acteur_name" : acteurName,
				"old_version" : old_version,
				"new_version" : new_version,
				"champ" : champ,
			})


